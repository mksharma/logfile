<?php

/////////////////////////////////////define standerd variable (Set Mode) ///////////////////////////

define('DEBUG',1);
define('INFO',1);
define('ERROR',1);

////////////////////////////////////////////Logging class////////////////////////////////////////

class Logging extends Exception{
	
	private static $log=NULL;
	 
	public static function getInstance()
	{
		if(self::$log==NULL)
		{
			self::$log=new Logging();
		}
		return self::$log;
	}

	
	public function debug($debugFileName,$debugArrayData)
	{		
	    if(file_exists($debugFileName))
		{  
		    //check set property
		   if(DEBUG==1)
		   {	
		     //check File is writable or not		   
			if(!is_writable($debugFileName)) chmod($debugFileName ,0777);
				       
			$fileNm = fopen($debugFileName ,"a+");
			//Check file data in array or string	
			if(is_array($debugArrayData)){   			
			fwrite($fileNm , '[DEBUG] '.date("d-m-Y h:i:s").' - Debug Log:'.PHP_EOL.print_r($debugArrayData,true).PHP_EOL.'-----------------------------------------'.PHP_EOL);
			}else{
		     fwrite($fileNm , '[DEBUG] '.date("d-m-Y h:i:s").' - Debug Log:'.PHP_EOL.$debugArrayData.PHP_EOL.'-----------------------------------------'.PHP_EOL);
			}				
			fclose($fileNm);
		   }	
		
		} else {			
		     echo "The file $filename does not exist";
		}		
	}
	public  function info($infoFileName,$infoArrayData)
	{
		
		if(file_exists($infoFileName))
		{  
		    //check set property
		   if(INFO==1)
		   {
			   //check File is writable or not
			if(!is_writable($infoFileName)) chmod($infoFileName ,0777);			   		       
			$fileNm = fopen($infoFileName ,"a+");
			//Check file data in array or string			
			if(is_array($infoArrayData)){   	
			fwrite($fileNm , '[INFO] '.date("d-m-Y h:i:s").' - Info Log:'.PHP_EOL.print_r($infoArrayData,true).PHP_EOL.'-----------------------------------------'.PHP_EOL);
			}else{
			fwrite($fileNm , '[INFO] '.date("d-m-Y h:i:s").' - Info Log:'.PHP_EOL.$infoArrayData.PHP_EOL.'-----------------------------------------'.PHP_EOL);				
			}	
			fclose($fileNm);
			chmod($infoFileName ,0777);
		   }
		} else {			
		     echo "The file $filename does not exist";
		}			
	}
	
	public function error($errorFileName,$errorArrayData,$exception_log=NULL)
	{
		  //check File is exists or not
		if(file_exists($errorFileName))
		{
			//check set property
		   if(ERROR==1)
		   {        
		           //check File is writable or not
			       if(!is_writable($errorFileName)) chmod($errorFileName ,0777);
				   $errorMsg=array();	       
			       $fileNm = fopen($errorFileName ,"a+");			
			       //$exception = new Exception; // Now using php default class Exception for Exception handling
					$logMsg[] = "Message: " . $this->getMessage() ;
					$traceAsString[]= "File: " . $this->getFile();
					$traceLine[]= "Line: " . $this->getLine();
					$trace[]= "Trace: " . $this->getTraceAsString() ;
		
			        //Check file data in array or string and check null
			        $errorData=array();
					if(!is_array($errorArrayData))
					{   
						$errorData[]=$errorArrayData;
					}else{
						$errorData=$errorArrayData;
					}			
					if($exception_log!=null)
					{
							$exceptionLog=array();
							if(!is_array($exception_log))
							{   
								$exceptionLog[]=$exception_log;
							}else{
								$exceptionLog=$exception_log;
							}										
							$errorArrayData=array_merge($errorData, $exceptionLog, $logMsg, $traceAsString,$traceLine,$trace);
					}else{
							$errorArrayData=array_merge($errorData, $logMsg, $traceAsString,$traceLine,$trace);	
					}
			        //check array data
			          fwrite($fileNm , '[ERROR] '.date("d-m-Y h:i:s").' - Error Log:'.PHP_EOL.print_r($errorArrayData,true).PHP_EOL.'-----------------------------------------'.PHP_EOL);	
			         //exception_log.printstacktrace();			
			          fclose($fileNm);
			          chmod($errorFileName ,0777);
		   	   }	
		
		  } else {			
			 echo "The file $filename does not exist";
		 }		
	 }
}

?>