/////////////////////////////////////////////////////////////////////////////

Read me for Generate log file
 
Version 1.0

Main Purpose:- logging file main purpose are find easily in program error/hidden error and we can debug program easily. It is debugging class file, you can use in php any CMS and framework like wordpress,cakephp etc. You can include(or extends / import) logging.php in your program file and use their methods as error,info,debug according to your requirement. 

Benefits:- We can save our time and can view our debug, error or info in logs file, output will be in array format. Logs file will display using type of methods ,date time, message in array format. so we can view and easy to understand any message.

Features:- Singleton based application, Follow oops property. 

Generate log file using logging class with their method, so we have following step , they are-

Step 1- In your Project, Include class file logging.php in your working file or globally access file like header.

Step 2- Set your Property value on top of class file (logging.php).

         Meaning of property variable--

	DEBUG 1--> Generate debug log text ||  0 --> Not any Activity ,No debug  log text generate 
	INFO  1--> Generate Info log text  ||  0 --> Not any Activity ,No Info log text generate 
	ERROR 1--> Generate Error log text ||  0 --> Not any Activity ,No Error log text generate 

Step 3- Create blank log file on root path or your choice folder.

Step 4- Call class with object like  $log = Logging::getInstance();

Step 5:- Call method according your requirement by passing your argument as syntax-

	      $log->error($fileName, $argument1 , $argument2);  //	$argument2 is optional
                    
		  $log->info($fileName, $argument1); 

          $log->debug($fileName, $argument1); 

 Note: Give proper path of your file using $fileName variable.You can pass first argument logs file name with their path, if you have put logs file in root than mentioned only file name. Second argument is error/info/debug message( it can be array or string), in error method third argument is optional.









        

